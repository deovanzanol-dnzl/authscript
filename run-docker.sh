#!/bin/bash
echo on
clear
echo '###########################################################'
echo '#Script para configurar login no docker e donwload da     # '
echo '#imagem                                                   #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo ''

espaco() {
  echo ''
  echo '###########################################################'
  echo ''
}

portServer=$1
portApplication=$2
ambiente=$3
aplicacao=$4
filePath="c:\windows\system32\drivers\etc\hosts"
hostIP="127.0.1.1 localhost.prisma.rumolog.com"

echo "Total arguments : $*"
echo "Porta server : $portServer"
echo "Porta Application : $portApplication"
echo "Ambiente : $ambiente"

espaco
echo 'Verificando arquivo host'
echo $(grep -Fxq "$hostIP" $filePath)
if grep -Fxq "$hostIP" $filePath; then ##note the space after the string you are searching for
  echo "Já existe o host  $hostIP"
else
  echo 'Add  127.0.1.1 localhost.prisma.rumolog.com in  c:\windows\system32\drivers\etc\hosts'
  echo $hostIP >>$filePath
fi
espaco
echo 'Normalize path'
find='/'
replace='\'
pathKey=$(echo "$PWD/arquivokey.key")
pathKey=${pathKey///c/C:}
pathKey=${pathKey///d/D:}

pathCrt=$(echo "$PWD/arquivocrt.crt")
pathCrt=${pathCrt///c/C:}
pathCrt=${pathCrt///d/D:}

espaco

echo 'Stop docker container'
echo $(docker stop $aplicacao)
echo 'Rm docker container'
echo $(docker rm $aplicacao)

espaco
echo 'Run docker containes'
comando='docker run \
  --name '$aplicacao' \
  -p '$portServer':8443 \
  -e "PROMETHEUS_PATH=//metrics" \
  -e "SERVER=localhost.prisma.rumolog.com" \
  -e "UPSTREAM=http://host.docker.internal:'$portApplication'" \
  -e "POST_LOGOUT_URI=https://localhost.prisma.rumolog.com:'$portServer'" \
  -e "OIDC_DISCOVERY_ENDPOINT=https://sso-dev.prisma.rumolog.com/auth/realms/tlg/.well-known/openid-configuration" \
  -e "REDIRECT_URI=https://localhost.prisma.rumolog.com:'$portServer'/redirect_uri" \
  -e "PKCE_CODE_VERIFIER=mUkQ1u8DRhdunOfFMdBwIiHnTN6qKjWMwhpJkohmB1egcx8yrNQ225vVA8wahTx63a64e2iNs7JEHovh" \
  -e "CLIENT_ID=frontend-developers" \
  -v '\"$pathKey\"':/var/tls/private/tls.key \
  -v '\"$pathCrt\"':/var/tls/private/tls.crt \
  --rm "registry.prisma.rumolog.com/ci-tools/oidc-proxy:'$ambiente'"'
espaco
echo -e ${comando//\\/}
espaco
echo "Acesse -> https://localhost.prisma.rumolog.com:$portServer"

espaco
eval "$comando"
