#!/bin/bash
echo on
clear
echo '###########################################################'
echo '#Script para configurar login no docker e donwload da     # '
echo '#imagem                                                   #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo ''

espaco() {
  echo ''
  echo '###########################################################'
  echo ''
}

usuario=$1
senha=$2
ambiente=dev

echo "Total arguments : $*"
espaco
echo 'copy daemon.json'
cp daemon.json ~/.docker/daemon.json
echo 'Copiado com sucesso'

#echo 'Reiniciando docker'
#net stop "com.docker.service" & net start "com.docker.service"
#sc stop com.docker.service && sc start com.docker.service
espaco
echo 'Executando login em : https://paas.rumolog.com/'
oc login https://paas.rumolog.com/ -u $usuario -p $senha
espaco
echo 'Efetuando checkout no projeto prisma-dev'
oc project prisma-dev 
espaco
echo 'Gerando token Whoami -t'
token=$(oc whoami -t)
echo "token : $token"
espaco
echo 'Docker login'
docker login -p $token -u $usuario registry.prisma.rumolog.com
espaco
echo 'Gerando token config.json'
tokenConfig=$(echo -n $usuario:$token | base64)
echo "tokenConfig : $tokenConfig"
config=$(echo -n '{"auths": {"registry.prisma.rumolog.com": {"auth": "'$tokenConfig'"}},"HttpHeaders": {"User-Agent": "Docker-Client/19.03.5 (windows)"},"credsStore": "desktop","stackOrchestrator": "swarm"}')
echo "$config" >>config.json
cp -rf config.json ~/.docker/config.json
rm config.json

espaco
echo 'Pull da imagem'
docker pull registry.prisma.rumolog.com/ci-tools/oidc-proxy:$ambiente

echo "Finalizado"
sleep 10