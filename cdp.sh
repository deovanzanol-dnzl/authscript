#!/bin/bash
echo on
clear
echo '###########################################################'
echo '#Script executar container Controle de perdas             # '
echo '#                                                         #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo ''

espaco() {
  echo ''
  echo '###########################################################'
  echo ''
}

portServer=7443
portApplication=4400
ambiente=dev
aplicacao=cdp

bash ./run-docker.sh $portServer $portApplication $ambiente $aplicacao
