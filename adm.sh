#!/bin/bash
echo on
clear
echo '###########################################################'
echo '#Script executar container Administração                  # '
echo '#                                                         #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo ''

espaco() {
  echo ''
  echo '###########################################################'
  echo ''
}

portServer=9443
portApplication=4300
ambiente=dev
aplicacao=adm

bash ./run-docker.sh $portServer $portApplication $ambiente $aplicacao
