# Requisitos
 
 * Docker desktop deve estar atualizado e em execução.
 * Estar conectado na VPN da RUMO.
 * Executar os projetos de Front-End com o comando 'npm run start-local'.

# Passos

### Open Shift Client

 *  Baixar Openshift client em https://www.okd.io/download.html. 
 *  Extrair o zip em algum diretório e adicionar este diretório na variável PATH do Windows

### Para efetuar pull da imagem do oidc-proxy:

*   Copiar o arquivo daemon.json para C:\Users\<4letras>\.docker\daemon.json
*   Reinicar o docker
*   Conectar na VPN da RUMO.
*   Abrir o git bash e navegar até o diretório do projeto authScript.
*   Executar o script ./pull-image-oidc-proxy.sh 'TRXXXXXX' 'SENHA'

Pronto! Com isso a imagem do registry.prisma.rumolog.com/ci-tools/oidc-proxy deve estar disponível.

## Scripts:

####  Agora para executar os containers basta executar os script abaixos descritos.(Executar como administrador pelo menos uma vez, para adicionar o endereço  '127.0.1.1 localhost.prisma.rumolog.com' no arquivo  'c:\windows\system32\drivers\etc\hosts'):

*  ./cdp.sh -> Para executar o Controle de Perdas na porta 7443
*  ./sro.sh -> Para executar o SRO na porta 8443
*  ./adm.sh -> Para executar o Administração na porta 9443
