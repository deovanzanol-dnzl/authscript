#!/bin/bash
echo on
clear
echo '###########################################################'
echo '#Script executar container SRO                            # '
echo '#                                                         #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo ''

espaco() {
  echo ''
  echo '###########################################################'
  echo ''
}

portServer=8443
portApplication=4200
ambiente=dev
aplicacao=sro

bash ./run-docker.sh $portServer $portApplication $ambiente $aplicacao
